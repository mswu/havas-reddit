package com.msw.havasreddit

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.msw.havasreddit.model.Feed
import com.msw.havasreddit.model.Post

class MainViewModel(application: Application) : AndroidViewModel(application) {
    companion object {
        private const val END_POINT = "https://www.reddit.com/.json"
        private val TAG = MainViewModel::class.java.simpleName
    }

    private val queue = Volley.newRequestQueue(application)
    private val gson = Gson()
    private val _posts = MutableLiveData<List<Post>>()

    val posts: LiveData<List<Post>> get() = _posts

    override fun onCleared() {
        super.onCleared()
        queue.cancelAll(TAG)
    }

    fun fetch() {
        queue.cancelAll(TAG)
        val request = StringRequest(END_POINT, { response ->
            try {
                val reddit = gson.fromJson(response, Feed::class.java)
                _posts.postValue(reddit.data.children.map { it.data })
            } catch (e: JsonSyntaxException) {
                Log.e(TAG, e.toString())
            }
        }, { error ->
            Log.w(TAG, error)
        })
        queue.add(request)
    }
}