package com.msw.havasreddit

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.msw.havasreddit.model.Post
import kotlinx.coroutines.*

class PostView(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0, defStyleRes: Int = 0) : FrameLayout(context, attrs, defStyleAttr, defStyleRes) {
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(context, attrs, defStyleAttr, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context) : this(context, null)

    init {
        inflate(context, R.layout.view_post, this)
    }

    fun bind(post: Post) {
        findViewById<TextView>(R.id.title_text).text = post.title
        findViewById<TextView>(R.id.ups_text).text = resources.getString(R.string.ups, post.ups)
        findViewById<TextView>(R.id.comments_text).text = resources.getString(R.string.comments, post.numComments)
        post.thumbnail?.let { url ->
            CoroutineScope(Dispatchers.IO).launch {
                downloadThumbnail(url)
            }
        }
    }

    private suspend fun downloadThumbnail(url: String) {
        getBitmapFromUrl(url)?.let { bitmap ->
            withContext(Dispatchers.Main) {
                findViewById<ImageView>(R.id.thumbnail_image).also {
                    it.setImageBitmap(bitmap)
                }
            }
        }
    }
}