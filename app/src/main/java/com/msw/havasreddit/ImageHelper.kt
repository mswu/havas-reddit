package com.msw.havasreddit

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.net.URL

fun getBitmapFromUrl(link: String): Bitmap? = try {
    val connection = URL(link).openConnection().apply {
        doInput = true
        connect()
    }
    val input = connection.getInputStream()
    BitmapFactory.decodeStream(input)
} catch (e: Exception) {
    null
}