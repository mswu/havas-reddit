package com.msw.havasreddit.model

import android.os.Parcel
import android.os.Parcelable

data class Preview(val images: List<ImageObject>?) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.createTypedArrayList(ImageObject)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(images)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Preview> {
        override fun createFromParcel(parcel: Parcel): Preview {
            return Preview(parcel)
        }

        override fun newArray(size: Int): Array<Preview?> {
            return arrayOfNulls(size)
        }
    }
}