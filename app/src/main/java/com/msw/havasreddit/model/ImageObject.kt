package com.msw.havasreddit.model

import android.os.Parcel
import android.os.Parcelable

data class ImageObject(val source: Image?, val resolutions: List<Image?>?) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(Image::class.java.classLoader),
        parcel.createTypedArrayList(Image)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(source, flags)
        parcel.writeTypedList(resolutions)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ImageObject> {
        override fun createFromParcel(parcel: Parcel): ImageObject {
            return ImageObject(parcel)
        }

        override fun newArray(size: Int): Array<ImageObject?> {
            return arrayOfNulls(size)
        }
    }
}