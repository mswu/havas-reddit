package com.msw.havasreddit.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Post(
    val title: String?,
    val ups: Int,
    val downs: Int,
    val thumbnail: String?,
    @SerializedName("thumbnail_width")
    val thumbnailWidth: Int,
    @SerializedName("thumbnail_height")
    val thumbnailHeight: Int,
    @SerializedName("num_comments")
    val numComments: Int,
    val preview: Preview?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readParcelable(Preview::class.java.classLoader)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeInt(ups)
        parcel.writeInt(downs)
        parcel.writeString(thumbnail)
        parcel.writeInt(thumbnailWidth)
        parcel.writeInt(thumbnailHeight)
        parcel.writeInt(numComments)
        parcel.writeParcelable(preview, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Post> {
        override fun createFromParcel(parcel: Parcel): Post {
            return Post(parcel)
        }

        override fun newArray(size: Int): Array<Post?> {
            return arrayOfNulls(size)
        }
    }
}