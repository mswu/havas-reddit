package com.msw.havasreddit.model

data class RedditData(val children: List<ChildrenData>)