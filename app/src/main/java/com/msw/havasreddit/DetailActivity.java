package com.msw.havasreddit;

import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.msw.havasreddit.databinding.ActivityDetailBinding;
import com.msw.havasreddit.model.Image;
import com.msw.havasreddit.model.ImageObject;
import com.msw.havasreddit.model.Post;

public class DetailActivity extends AppCompatActivity {
    public final static String ARG_POST = "post";

    private ActivityDetailBinding binding;
    private Post post;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        post = getIntent().getParcelableExtra(ARG_POST);

        if (post != null) {
            binding.titleText.setText(post.getTitle());
            binding.upsText.setText(getString(R.string.ups, post.getUps()));
            binding.commentsText.setText(getString(R.string.comments, post.getNumComments()));

            if (post.getPreview() != null && post.getPreview().getImages() != null && !post.getPreview().getImages().isEmpty()) {
                final ImageObject imageObject = post.getPreview().getImages().get(0);
                final Image image = imageObject.getSource();

                if (image != null && image.getUrl() != null) {
                    new Thread(() -> {
                        Bitmap bitmap = ImageHelperKt.getBitmapFromUrl(image.getUrl().replace("amp;", ""));
                        if (bitmap != null) {
                            runOnUiThread(() -> binding.thumbnailImage.setImageBitmap(bitmap));
                        }
                    }).start();
                }
            }
        }
    }
}