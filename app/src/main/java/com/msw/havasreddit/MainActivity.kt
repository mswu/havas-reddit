package com.msw.havasreddit

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.msw.havasreddit.databinding.ActivityMainBinding
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val viewModel: MainViewModel by viewModels()
    private val overlay = OverlayDialogFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        overlay.show(supportFragmentManager, null)
        viewModel.posts.observe(this) { posts ->
            lifecycleScope.launch {
                posts.forEach { post ->
                    val view = PostView(applicationContext)
                    view.setOnClickListener {
                        val intent = Intent(this@MainActivity, DetailActivity::class.java).apply {
                            putExtra(DetailActivity.ARG_POST, post)
                        }
                        startActivity(intent)
                    }
                    runOnUiThread {
                        view.bind(post)
                        binding.grid.addView(view)
                    }
                }
            }
            overlay.dismiss()
        }
        viewModel.fetch()
    }
}